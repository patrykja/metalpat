var http = require("http");
var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");
var crypto = require('crypto');
var multer = require("multer");
var mime = require('mime');
var fs = require('fs');

var app = express();

var session = require("express-session");

app.set('port', (process.env.PORT || 1337));


app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
app.use(logger('dev'));
app.use(bodyParser.urlencoded({'extended': 'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json

app.locals.users = [];
app.locals.flag = false;


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.get("/eu/mgo2/policy/policy.txt", function (req, res) {
	res.sendFile(__dirname + '/public/policy.txt');
});

app.get("/eu", function (req, res) {
	res.sendFile(__dirname + '/public/policy.txt');
});

app.get("/monet", function (req, res) {
	res.send("MONET");
});



app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});